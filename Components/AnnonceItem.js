import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'

import AnnonceDetail from './AnnonceDetail'
import Annonce from '../Helpers/AnnoncesData'

class AnnonceItem extends React.Component {

  render() {
    const { Annonce, displayDetailForAnnonce } = this.props
    return (

        <TouchableOpacity
          style={styles.main_container}
          onPress={() => displayDetailForAnnonce(Annonce.id)}>
          <Image
            style={styles.image}
            source={{uri: "image"}}
          />
          <View style={styles.content_container}>
              <Text style={styles.title_text} numberOfLines={2}>{Annonce.title}</Text>
              <Text style={styles.localisation}>{Annonce.localisation}</Text>
              <Text style={styles.description_text} numberOfLines={2}>{Annonce.overview}</Text>
              <Text style={styles.date_text}>{Annonce.date}</Text>
          </View>
        </TouchableOpacity>
      )
    }
  }

const styles = StyleSheet.create({
    main_container: {
      height: 150,
      flexDirection: 'row',
      margin: 5,
      backgroundColor: 'white',
      borderRadius: 10,
      alignItems: 'center',
    },
    image: {
      width: 100,
      height: 100,
      borderRadius: 100/2,
      backgroundColor: 'pink',
      margin: 5,
    },
    content_container: {
      flex: 1,
      margin: 5,
      flexDirection: 'column',
    },
    ligne_un: {
      flex: 3,
      flexDirection: 'row'
    },
    title_text: {
      fontWeight: 'bold',
      color: '#48c9b0',
      fontSize: 20,
    },
    localisation: {
      fontWeight: 'bold',
      fontSize: 14,
      color: 'pink'
    },
    description_container: {
      flex: 7
    },
    description_text: {
      fontStyle: 'italic',
      color: '#666666'
    },
    date_text: {
      fontSize: 14,
      color: '#48c9b0'
    }
  })

export default AnnonceItem
