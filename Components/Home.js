import React from 'react'
import { TouchableOpacity, StyleSheet, View, TextInput, Button, FlatList, Text, Alert  } from 'react-native'
import InfoButton from './InfoButton'

class Home extends React.Component {

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={{ flex:1, flexDirection: 'column' }}>
        <TouchableOpacity onPress={() => navigate('AnnoncesVert')} style={styles.top_container}>
          <View style={styles.header}>
            <InfoButton
              text="info"
              onPress={() => {
                alert("Hi there!!!");
              }}
            />
          </View>
          <View style={styles.content}>
            <Text style={styles.texte}>
              JE CHERCHE UN "CLEANER"
            </Text>
          </View>
          <View style={styles.footer}>

          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate('AnnoncesBleu')} style={styles.bottom_container}>
          <View style={styles.header}>
            <InfoButton
              text="info"
              onPress={() => {
                alert("Hi there!!!");
              }}
            />
          </View>
          <View style={styles.content}>
            <Text style={styles.texte}>
                JE SUIS UN "CLEANER"
            </Text>
          </View>
          <View style={styles.footer}>

          </View>
        </TouchableOpacity>
      </View>
    )
  }
}



const styles = ({
  top_container: {
    flex:1,
    flexDirection: 'column',
    backgroundColor: '#48c9b0',

  },
  bottom_container: {
    flex:1,
    flexDirection: 'column',
    backgroundColor: '#5d94ce'
  },
  texte: {
    fontWeight: 'bold',
    color: 'white',
    fontSize: 21,
  },
  content: {
  flex: 5,
  alignItems: 'center',
  justifyContent: 'center',
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    margin:10,
    alignItems: 'flex-end' ,
    justifyContent: 'flex-end'
  },
  footer: {
    flex: 1,
    flexDirection: 'row',
    margin:10,
    alignItems: 'flex-end',
    justifyContent: 'flex-end' 
  },
})

export default Home
