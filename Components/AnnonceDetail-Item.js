import React from 'react'
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native'

import AnnonceDetail from './AnnonceDetail'
import Annonce from '../Helpers/AnnoncesData'

class AnnonceDetailItem extends React.Component {

  render() {
    const { Annonce, displayDetailForAnnonce } = this.props
    return (

        <View>
          <Image
            style={styles.image}
            source={{uri: "image"}}
          />
          <View style={styles.content_container}>
            <View style={styles.header_container}>
              <Text style={styles.title_text}>{Annonce.title}</Text>
              <Text style={styles.vote_text}>{Annonce.annonceur}</Text>
            </View>
            <View style={styles.description_container}>
              <Text style={styles.description_text} numberOfLines={6}>{Annonce.overview}</Text>
              {/* La propriété numberOfLines permet de couper un texte si celui-ci est trop long, il suffit de définir un nombre maximum de ligne */}
            </View>
            <View style={styles.date_container}>
              <Text style={styles.date_text}>publiée le {Annonce.release_date}</Text>
            </View>
          </View>
        </View>
      )
    }
  }

const styles = StyleSheet.create({
    main_container: {
      height: 190,
      flexDirection: 'row',
    },
    image: {
      width: 120,
      height: 150,
      margin: 5,
      backgroundColor: 'gray'
    },
    content_container: {
      flex: 1,
      margin: 5
    },
    header_container: {
      flex: 3,
      flexDirection: 'row'
    },
    title_text: {
      fontWeight: 'bold',
      color: '#48c9b0',
      fontSize: 20,
      flex: 1,
      flexWrap: 'wrap',
      paddingRight: 5
    },
    vote_text: {
      fontWeight: 'bold',
      fontSize: 14,
      color: '#666666'
    },
    description_container: {
      flex: 7
    },
    description_text: {
      fontStyle: 'italic',
      color: '#666666'
    },
    date_container: {
      flex: 1
    },
    date_text: {
      textAlign: 'right',
      fontSize: 14
    }
  })

export default AnnonceDetailItem
