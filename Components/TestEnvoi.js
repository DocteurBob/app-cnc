import React, { Component } from 'react';
import { AppRegistry, StyleSheet, TextInput, View, Alert, Button, Text } from 'react-native'

class TestEnvoi extends Component {
  constructor(props) {
    super(props)
    this.state = {
      Data:'',
    }
  }

  EnvoiFunction=()=>{
    const{Data}=this.state;

    const jsonTest = {"un":"test un"};
    fetch('http://10.0.2.2/testDB/testEnvoi.php',{
      method:'POST',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json',
      },
      body:JSON.stringify({
        data:jsonTest,
      })
    }).then((response)=>response.json())
  }

  render(){
    return(
      <View style={styles.MainContainer}>
        <TextInput placeholder="TAPEZ ICI" onChangeText={Data=>this.setState({Data})}/>
        <Button title="Envoyer" onPress={this.EnvoiFunction} color="#2196F3" />
      </View>
    )
  }
}

export default TestEnvoi

const styles = StyleSheet.create({
  MainContainer :{
    justifyContent: 'center',
    flex:1,
    margin: 10
  },
})
