import React from 'react'
import { View, TextInput, Button, FlatList, Text } from 'react-native'

import Annonce from '../Helpers/AnnoncesData'
import AnnonceItem from './AnnonceItem'
import AnnonceDetail from './AnnonceDetail'

class AnnoncesVert extends React.Component {
  _displayDetailForAnnonce = (idAnnonce) => {
    this.props.navigation.navigate("AnnonceDetail", { idAnnonce: idAnnonce})
  }
  render() {
    return (
      <View style={{ backgroundColor:'#eff1f4' }}>
        <FlatList
          data={Annonce}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({item}) => <AnnonceItem Annonce={item} displayDetailForAnnonce={this._displayDetailForAnnonce}/>}
        />
      </View>
    )
  }
}

const style = {
  textinput: {
    marginLeft: 5,
    marginRight: 5,
    height: 50,
    borderColor: '#000000',
    borderWidth: 1,
    paddingLeft: 5
  }
}


export default AnnoncesVert
