import React, {Component} from 'react'
import { View, Image, TouchableOpacity, Text } from 'react-native'
import {
  createDrawerNavigator,
  createStackNavigator,
  createAppContainer,
  DrawerItems
} from 'react-navigation'


import AnnoncesVert from '../Components/AnnoncesVert'
import AnnoncesBleu from '../Components/AnnoncesBleu'
import ResultRecherche from '../Components/ResultRecherche'
import Home from '../Components/Home'
import AnnonceDetail from '../Components/AnnonceDetail'
import Connection from '../Components/Connection'
import TestRequete from '../Components/TestRequete'
import TestEnvoi from '../Components/TestEnvoi'

class NavigationDrawerStructure extends Component {
  //Structure for the navigatin Drawer
  toggleDrawer = () => {
    //Props to open/close the drawer
    this.props.navigationProps.toggleDrawer();
  };
  render() {
    return (
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity onPress={this.toggleDrawer.bind(this)}>
          {/*Donute Button Image */}
          <Image
            source={require('../img/drawer-150x150.png')}
            style={{ width: 30, height: 30, marginLeft: 5}}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

class DrawerComponent extends Component {

}


const DrawerContent = (props) => (
  <View>
    <View
      style={{

        height: 140,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Image
        source={require('../img/logo.jpg')}
        style={{ width: 200, height: 200, marginTop:40}}
      />
    </View>
    <DrawerItems {...props} />
  </View>
)

const StackNav = createStackNavigator({
  Home:{
    screen: Home,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <NavigationDrawerStructure navigationProps={navigation} />,
      headerStyle: {},
    }),
  },
  AnnoncesVert: {
    screen: AnnoncesVert,
    navigationOptions: {
      title: 'Annonces',
      headerStyle: {
        backgroundColor: '#48c9b0',
      },
    }
  },
  AnnoncesBleu: {
    screen: AnnoncesBleu,
    navigationOptions: {
      title: 'Annonces',
      headerStyle: {
        backgroundColor: '#5d94ce',
      },
    }
  },
  AnnonceDetail: {
    screen: AnnonceDetail,
    navigationOptions: {
      title: "Detail de l'annonce",
      headerStyle: {
        backgroundColor: '#48c9b0',
      },
    }
  },
})

const DrawerNav = createDrawerNavigator(
  //Drawer Optons and indexing
  {
    Home: {
      //Title
      screen: StackNav,
      navigationOptions: {
        drawerLabel: 'Consulter les annonces',
      }
    },
    // AnnonceDetail:{
    //   //Title
    //   screen: AnnonceDetail,
    //   navigationOptions: {
    //     drawerLabel: 'Offres',
    //   }
    // },
    PublierAnnonce:{
      screen: AnnonceDetail,
      navigationOptions:{
        drawerLabel: 'Publier une annonce'
      }
    },
    Connection:{
      screen: Connection,
      navigationOptions:{
        drawerLabel: 'Mon compte'
      }
    },
    AnnonceSave:{
      screen: AnnonceDetail,
      navigationOptions:{
        drawerLabel: 'Annonces sauvergardées'
      }
    },
    MesAnnonces:{
      screen: AnnonceDetail,
      navigationOptions:{
        drawerLabel: 'Mes annonces'
      }
    },
    TestRequete: {
      screen: TestRequete,
      navigationOptions: {
        drawerLabel: "test requête",
      }
    },
    TestEnvoi: {
      screen: TestEnvoi,
      navigationOptions: {
        drawerLabel: "test envoi",
      }
    },
  },
  {contentComponent: DrawerContent}
)



export default createAppContainer(DrawerNav)
